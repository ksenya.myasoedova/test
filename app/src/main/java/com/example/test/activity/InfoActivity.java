package com.example.test.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.test.R;

public class InfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });
    }
}
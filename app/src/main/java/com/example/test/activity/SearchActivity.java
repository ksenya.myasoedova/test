package com.example.test.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.test.R;

public class SearchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        findViewById(R.id.backFromSearchActivity).setOnClickListener(v -> {
            finish();
        });

        findViewById(R.id.test).
                setOnClickListener(v -> {
                    startActivity(
                            new Intent(SearchActivity.this,
                                    NotEmptySearchActivity.class
                            ));
                });
    }
}
package com.example.test.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.test.R;

public class SignUpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        EditText signUpEmail = findViewById(R.id.signUpEmail);
        EditText signUpPassword = findViewById(R.id.signUpPassword);
        EditText signUpName = findViewById(R.id.signUpName);

        findViewById(R.id.signUpToMain).
                setOnClickListener(v -> {
                    String email = signUpEmail.getText().toString();
                    String password = signUpPassword.getText().toString();
                    if (email.length() == 0)
                        signUpName.setError("Введите Name");

                    else if (email.length() == 0)
                        signUpEmail.setError("Введите Email");

                    else if (password.length() == 0)
                        signUpPassword.setError("Введите Password");

                    else
                        startActivity(
                                new Intent(SignUpActivity.this,
                                        MainActivity.class
                                ));
                });

        findViewById(R.id.toSignIn).
                setOnClickListener(v -> {
                    startActivity(
                            new Intent(SignUpActivity.this,
                                    SignInActivity.class
                            ));
                });
    }
}

package com.example.test.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.test.R;

public class NotEmptySearchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_not_empty_search);

        findViewById(R.id.backFromFavoriteActivity).setOnClickListener(v -> {
            finish();
        });
    }
}
package com.example.test.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.test.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.fromAnimestoInfo).
                setOnClickListener(v -> {
                    startActivity(
                            new Intent(MainActivity.this,
                                    InfoActivity.class
                            ));
                });

        findViewById(R.id.fromMangastoInfo).
                setOnClickListener(v -> {
                    startActivity(
                            new Intent(MainActivity.this,
                                    InfoActivity.class
                            ));
                });

        findViewById(R.id.fromRanobetoInfo).
                setOnClickListener(v -> {
                    startActivity(
                            new Intent(MainActivity.this,
                                    InfoActivity.class
                            ));
                });

        findViewById(R.id.toSearchActivity).
                setOnClickListener(v -> {
                    startActivity(
                            new Intent(MainActivity.this,
                                    SearchActivity.class
                            ));
                });

        findViewById(R.id.toFavoriteActivity).
                setOnClickListener(v -> {
                    startActivity(
                            new Intent(MainActivity.this,
                                    FavoriteActivity.class
                            ));
                });
    }
}

package com.example.test.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.test.NotEmptyFavoriteActivity;
import com.example.test.R;

public class FavoriteActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);

        findViewById(R.id.backFromFavoriteActivity).setOnClickListener(v -> {
            finish();
        });
        findViewById(R.id.toNotEmpty).
                setOnClickListener(v -> {
                    startActivity(
                            new Intent(FavoriteActivity.this,
                                    NotEmptyFavoriteActivity.class
                            ));
                });
    }
}